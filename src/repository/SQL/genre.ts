import { createGenre, findGenre } from "./repository-genre-sql";
import { Router, Request, Response } from "express";
const genre = Router();

//methode GET pour genre

genre.get("/", async (req:Request, res:Response) => {
  const genre1 = await findGenre();
  res.json(genre1);
});

//methode POST genre

genre.post("/", async (req:Request, res:Response) => {
  const genre2 = await createGenre(req.body);
  res.json(genre2);
});

export default genre;
