import { Router, Request, Response } from "express";
import { createFilm, findFilm } from "./repository-film";
const film = Router();

film.get('/api/film', async (req:Request, res:Response) => {
    const film1 = await findFilm();
    res.json(film1);
});

film.post('/api/film', async (req:Request, res:Response) => {
    const film2 = await createFilm([req.body]);
    res.json(film2);
});

export default film;