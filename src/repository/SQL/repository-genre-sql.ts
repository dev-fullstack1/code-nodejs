import prisma from "../../db/bdd";

export const createGenre = async (gender:any) => {
    const result = await prisma.genre.create({data:gender});
    return result;
};

export const findGenre = async () => {
    const result = await prisma.genre.findMany();
    return result;
};

