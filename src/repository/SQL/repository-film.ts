import prisma from "../../db/bdd";

export const createFilm = async (film:any) => {
    const result = await prisma.film.create({data:film});
    return result;
};

export const findFilm = async () => {
    const result = await prisma.film.findMany();
    return result;
};