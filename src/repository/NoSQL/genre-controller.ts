import { Router, Request, Response } from "express";
import schema from "./genre-model";
const router = Router();


router.get("/", (req: Request, res: Response) => {
  schema.find((err:any, docs:any) => {
    if (!err) res.send(docs);
    else console.log("Error to get data :" + err);
  });
});

router.post("/", (req: Request, res: Response) => {
  const schema1 = new schema({
    genre: req.body.genre
  });
  schema1.save((err:any, docs:any) => {
    if (!err) {
      res.send(docs);
    } else {
      console.log("Error to enter data :" + err);
    }
  });
});

export default router;