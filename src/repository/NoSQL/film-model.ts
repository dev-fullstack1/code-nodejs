const mongoose1 = require("mongoose");

export const filmModel = mongoose1.Schema({
    title: String,
    gender:{ type: mongoose1.Schema.Types.ObjectId, ref: 'schema' },
    author: String
});

const Model = mongoose1.model('Model', filmModel);
export default Model;