import { Router, Request, Response } from "express";
import Model from "./film-model";
const router2 = Router();


router2.get("/", (req: Request, res: Response) => {
    Model.find((err:any, docs:any) => {
    if (!err) res.send(docs);
    else console.log("Error to get data :" + err);
  });
});

router2.post("/", (req: Request, res: Response) => {
  const Model1 = new Model({
    title: req.body.title,
    gender: req.body.gender,
    author: req.body.author
  });
  Model1.save((err:any, docs:any) => {
    if (!err) {
      res.send(docs);
    } else {
      console.log("Error to enter data :" + err);
    }
  });
});

export default router2;