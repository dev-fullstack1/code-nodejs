import express from 'express';
const app = express();
const http = require("http");
const swaggerUi = require('swagger-ui-express');
const swaggerDocument = require(`../swagger.json`);
import router from "./repository/NoSQL/genre-controller";
require("./db/dbConfig");
import * as bodyParser from 'body-parser';
import router2 from "./repository/NoSQL/film-controller";
import genre from "./repository/SQL/genre";
import film from './repository/SQL/film';


app.use(express.json());
app.use(bodyParser.json());
app.use("/genre", router);
app.use("/film", router2);
app.use("/api-docs",
    swaggerUi.serve, 
    swaggerUi.setup(swaggerDocument)
);
app.use("/api/genre", genre);
app.use("/api/film", film);

app.set("port", process.env.PORT || 3000);
const server = http.createServer(app);

server.listen(process.env.PORT || 3000);

